/*global Backbone,$,Spinner,window,document,location*/
var keyword = 'target_hispanics';

var Article = Backbone.Model.extend({
     urlRoot: 'http://www.army.mil/api/packages/getpackagesbykeywords' +
         '?keywords=' + keyword
});

var Articles = Backbone.Collection.extend({
    model: Article,
    url: 'http://www.army.mil/api/packages/getpackagesbykeywords' +
        '?keywords=' + keyword
});

var articles = new Articles();

var ArticleGallery = Backbone.View.extend({
    initialize: function(options) {    
        for (var i = 0; i < options.rows; i++) {
            this.listenTo(articles, 'sync', this.load_articles);
        }
        //articles.url += "&offset=" + options.offset + "&count=" + options.count;
        articles.fetch(); 
    },
    load_articles: function() { 
        var news_stories = $('<div>').prop({
            'class': 'news_stories'
        });

        var num_articles = 2;

        for (var i = 0; i < num_articles; i++) {
            if (articles.length > 0) {
                news_stories.append(build_news_cell(articles.shift()));
            } 
        }
        $("#news_section .more_news_wrap").before(news_stories);
        $(".news_story").last().addClass("last_news_story");
    }
});

function build_news_cell(article) { 
    var opts = {
        lines: 13, // The number of lines to draw
        length: 20, // The length of each line
        width: 10, // The line thickness
        radius: 30, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        color: '#000', // #rgb or #rrggbb or array of colors
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    };
    var spinner = new Spinner(opts).spin();
    // refer to the spinner with: target.append(spinner.el);
    // then stop with spinner.stop();
    
    var page = $('body').prop('id');
    
    var window_width = parseInt($(window).width(), 10);
    
    var news_story = $('<div>').prop({
        'class': 'news_story'
    }).height(300);
    var max_index = article.get("images").length - 1;
    var random_index = Math.floor((Math.random() * max_index) + 0);
    var image = "";
    if(window_width < 769) {
        image = article.get("images")[random_index].url_size1;
    } else {
        image = article.get("images")[random_index].url_size2;
    }
    var alt = article.get("images")[random_index].alt;
    var title = article.get("title");
    var max_str_length = 55;
    if(title.length > max_str_length && window_width >= 481){
        title = title.substring(0, max_str_length);
        title += "...";
    }
    if(window_width < 481) {
        max_str_length = title.length;
    }
    var max_descriptions_length = 105;
    var url = article.get("page_url");
    var description = article.get("description").substring(0, max_descriptions_length) + "...";
    
    var image_url = url + '?from=moh_' + page + '_news_image';
    var title_url = url + '?from=moh_' + page + '_news_text';
    
    var news_story_image = $('<a>').prop({'href': image_url, "target": "_blank"});
    news_story.prepend(spinner.el);
    
    $('<img>').load(function() {
        news_story_image.append($(this));
        news_story.height('');
        spinner.stop();
    }).prop({
        'src': image,
        'alt': alt
    });
    var news_story_title = $('<h3>').prop({
        'class': ''
    }).append($('<a>').prop({
        'class': 'news_story_title',
        'href': title_url
    }).html(title));

    var news_story_description = $('<p>').prop({
        'class': 'news_story_description'
    }).html(description);

    return news_story.append(news_story_image).append(news_story_title).append(news_story_description);
}

$(document).ready(function() {

  if ($(".green-section").length) {
    var articleGallery;

    $(".green-section").waypoint(function() {
        // load articles when we scroll down past the battle section
        if (!articleGallery) {
            articleGallery = new ArticleGallery({
                rows: 3,
                offset: 1,
                count: 100
            });
        }
    }, {
        offset: 'bottom-in-view'
    });

    // bind click function to load more button
    $("#load_more_news").click(function(e) {
        e.preventDefault();
        for (var i = 0; i <= 2; i++) {
            articleGallery.load_articles();
        }
        if(articles.length === 0) {
            $(".news-section .more_news_wrap").hide();
        }
    });

  } else {

   //History page Smooth Scrolling
    $('#second-navbar ul li a').click(function(e) {
      e.preventDefault();
      var h = $('#main-nav').height() + $('#anchor_nav').height(),
      navbar_h = $('#second-navbar').height(), offset, target,
      title = $(this).text(), top_offset;
      offset = h - navbar_h;

      $('#second-navbar').collapse('hide');
      $('#nav-title').text(title);

      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

        if (target.length) {

          if (title == "1850") {
            top_offset = 0;
          } else if (!$('#anchor_nav').hasClass('affix')) {
            top_offset = target.offset().top - $('#civil_war').offset().top + 170;
          } else {
            top_offset = target.offset().top - offset + 120;
          }

          $('html,body').animate({
            scrollTop: top_offset
          }, 1000);
          return false;
        }
      }
    });

  }

});

//Mobile hamburger special effects
// document.querySelector( "#nav-toggle" ).addEventListener( "click", function() {
//   this.classList.toggle( "active" );
// });
$( "#nav-toggle" ).on( "click", function() {
  $( this ).toggleClass( "active" );
});

//parallax effects
(function(){ 
  // Browser control
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");

  var parallax = document.querySelectorAll(".parallax"),
  speed = 0.9;

  //only if min-width: 992px;
  if (msie <= 0 || parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10) > 9 ) {// If IE 10 or greater or if other than IE
        var mq = window.matchMedia( "(min-width: 992px)" );//media queries control
        if (mq.matches) {
           window.onscroll = function(){ 
              [].slice.call(parallax).forEach(function(el){
                var windowYOffset = window.pageYOffset,
                elBackgrounPos = "50% " + (windowYOffset * speed) + "px";
                el.style.backgroundPosition = elBackgrounPos;
                $(".main-text").css("opacity", 1 - $(window).scrollTop() / 950);
            });
          };
      }
  }
})();

//Rollover effects for profile images

$(document).ready(function(){
    $("#pcaption-1").hover(function(){
        $("#pimage-1").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/westphal.jpg');
        }, function(){
        $("#pimage-1").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/westphal-overlay.jpg');
    });
    
    $("#pcaption-2").hover(function(){
        $("#pimage-2").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/baca.jpg');
        }, function(){
        $("#pimage-2").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/baca-overlay.jpg');
    });
    
    $("#pcaption-3").hover(function(){
        $("#pimage-3").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/caldera.jpg');
        }, function(){
        $("#pimage-3").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/caldera-overlay.jpg');
    });
    
    $("#pcaption-4").hover(function(){
        $("#pimage-4").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/cavazos.jpg');
        }, function(){
        $("#pimage-4").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/cavazos-overlay.jpg');
    });
    
    $("#pcaption-5").hover(function(){
        $("#pimage-5").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/versace.jpg');
        }, function(){
        $("#pimage-5").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/versace-overlay.jpg');
    });
    
    $("#pcaption-6").hover(function(){
        $("#pimage-6").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/serna.jpg');
        }, function(){
        $("#pimage-6").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/serna-overlay.jpg');
    });
    
    $("#pcaption-7").hover(function(){
        $("#pimage-7").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/decastro.jpg');
        }, function(){
        $("#pimage-7").attr('src', 'http://usarmy.vo.llnwd.net/e2/rv5_images/hispanics/decastro-overlay.jpg');
    });
    
});

//History page sub-menu modifcations
//This code will find which div was clicked and replace the sub-menu's header with the correct title

/*
$('li#1850 a').click(function(){
	var width = $( window ).width();
	if (width < 768) {
		$( "span.submenu-header" ).html( "1850" );
		//$('#anchor_nav .navbar-toggle').click();
	}
});

$('li#1900 a').click(function(){
	var width = $( window ).width();
	if (width < 768) {
		$( "span.submenu-header" ).html( "1900" );
		//$('#anchor_nav .navbar-toggle').click();
	}
});

$('li#1950 a').click(function(){
	var width = $( window ).width();
	if (width < 768) {
		$( "span.submenu-header" ).html( "1950" );
		//$('#anchor_nav .navbar-toggle').click();
	}
});

$('li#2000 a').click(function(){
	var width = $( window ).width();
	if (width < 768) {
		$( "span.submenu-header" ).html( "2000-Present" );
		//$('#anchor_nav .navbar-toggle').click();
	}
});
*/

$('span#nav-title').click(function(){
	$( "span#nav-title" ).html( "1850 to Present" );
});

$('span.fa-caret-down').click(function(){
	$( "span#nav-title" ).html( "1850 to Present" );
});

$(".navbar").on("activate.bs.scrollspy", function(){
	var x = $(".nav li.active > a").text();
	var y = x.replace("History","");
	
	$( "span#nav-title" ).html( y );
})
